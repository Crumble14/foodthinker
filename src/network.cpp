#include "food_thinker.hpp"

using namespace FoodThinker;

Network::Network(const string file)
	: file{file}
{
	// TODO
}

void Network::set_inputs(const size_t inputs)
{
	this->inputs = inputs;
	// TODO Resize neurons on first layer
}

void Network::set_outputs(const size_t outputs)
{
	this->outputs = outputs;
	// TODO Resize neurons on last layer
}

void Network::set_layers(const size_t layers)
{
	this->layers.reserve(layers);

	for(size_t i = 0; i < layers; ++i) {
		this->layers.push_back(neurons_per_layer);
	}
}

void Network::set_neurons_per_layer(const size_t neurons)
{
	this->neurons_per_layer = neurons;

	for(size_t i = 0; i < layers.size(); ++i) {
		layers[i].resize(neurons_per_layer);
	}
}

vector<float> Network::activate(vector<float> inputs)
{
	if(layers.empty()) {
		vector<float> vec;
		vec.resize(outputs);

		return vec;
	}

	for(size_t i = 0; i < layers.size(); ++i) {
		inputs = layers[i].activate(inputs);
	}

	return inputs;
}

void Network::adjust(const float delta)
{
	for(size_t i = 0; i < layers.size(); ++i) {
		layers[i].adjust(delta);
	}
}

void Network::save() const
{
	// TODO
}
