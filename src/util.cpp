#include "food_thinker.hpp"

using namespace FoodThinker;

string FoodThinker::read_file(const string file)
{
	ifstream stream(file);
	if(!stream.is_open()) throw runtime_error("File not found!");

	stream.seekg(0, ios::end);
	const auto length = stream.tellg();
	stream.seekg(0);

	string buffer;
	buffer.resize(length);
	stream.read(&buffer[0], length);

	return buffer;
}
