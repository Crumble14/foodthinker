#include "food_thinker.hpp"

using namespace FoodThinker;

Layer::Layer(const size_t neurons)
{
	this->neurons.reserve(neurons);

	for(size_t i = 0; i < neurons; ++i) {
		this->neurons.emplace_back(neurons);
	}
}

void Layer::resize(const size_t neurons)
{
	while(neurons > this->neurons.size()) {
		this->neurons.emplace_back(neurons);
	}

	while(neurons < this->neurons.size()) {
		this->neurons.pop_back();
	}
}

vector<float> Layer::activate(const vector<float>& inputs)
{
	vector<float> outputs;
	outputs.resize(neurons.size());

	for(size_t i = 0; i < neurons.size(); ++i) {
		outputs[i] = neurons[i].activate(inputs);
	}

	return outputs;
}

void Layer::adjust(const float delta)
{
	for(size_t i = 0; i < neurons.size(); ++i) {
		neurons[i].adjust(delta);
	}
}
